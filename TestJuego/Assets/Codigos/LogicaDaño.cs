﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaDaño : MonoBehaviour
{
    //public LogicaBarraDeVida jugador;
    public LogicaBarraDeVida npc;
    public LogicaPersonaje logicaP;

    float daño = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.tag == "espadas" || other.tag == "personaje") && logicaP.estoyAtacando==true)
        {
            //jugador.vidaActual -= daño;
            npc.vidaActual -= daño;
        }
    }
}
