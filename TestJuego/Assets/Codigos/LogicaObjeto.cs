﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaObjeto : MonoBehaviour
{
    public bool destruirConCursor;
    public bool destruirAutomatico;
    public LogicaPersonaje logicaPersonaje;

    public int tipo;
    // 1=crece
    // 2=velocidad+
    // 3=salto+

    // Start is called before the first frame update
    void Start()
    {
        logicaPersonaje = GameObject.FindGameObjectWithTag("player").GetComponent<LogicaPersonaje>();    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Efecto() {
        switch (tipo) {
            case 1:
                logicaPersonaje.gameObject.transform.localScale = new Vector3(3, 3, 3);
                break;
            case 2:
                logicaPersonaje.velocidadInicial += 5;
                break;
            case 3:
                logicaPersonaje.fuerzaDeSalto += 10;
                break;
            default:
                Debug.Log("Sin Efecto");
                break;

        }
    }
}
