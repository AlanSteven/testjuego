﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class PresionandoCorrer : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public UnityEvent OnHold;
    public bool IsHolding;
    public LogicaPersonaje p;
    public void OnPointerDown(PointerEventData eventData)
    {
        IsHolding = true;
        //Debug.Log("true");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        IsHolding = false;
        //Debug.Log("false");
    }


    void Update()
    {
        if (IsHolding)
        {
            OnHold.Invoke();
            p.Corriendo();

        }
        else
        {
            p.Caminando();
        }
    }
}