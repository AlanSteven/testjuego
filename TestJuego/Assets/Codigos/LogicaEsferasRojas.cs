﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaEsferasRojas : MonoBehaviour
{
    public LogicaNPC logicaNPC;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "player")
        {
            logicaNPC.numDeObjetivos--;
            logicaNPC.textoMision.text = "obtén las esferas rojas" +
                                        "\n Restantes: " + logicaNPC.numDeObjetivos;

            if (logicaNPC.numDeObjetivos <= 0)
            {
                logicaNPC.textoMision.text = "Completaste la misión";
                logicaNPC.botonDeMision.SetActive(true);
            }
            transform.parent.gameObject.SetActive(false);
        }
    }
}
