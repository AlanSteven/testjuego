﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaPersonaje : MonoBehaviour
{
    //correr
    public int velCorrer;

    //caer mas rapido
    public int fuerzaExtra = 0;

    //velocidades
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 250.0f;

    public Animator anim;
    public float x, y;
    public SimpleTouchController left;
    public SimpleTouchController right;
    public Rigidbody rb;
    
    //salto
    public float fuerzaDeSalto = 8f;
    public bool puedoSaltar;

    //agachado
    public float velocidadInicial;
    public float velocidadAgachado;
    //cambio de collider
    public CapsuleCollider colParado;
    public CapsuleCollider colAgachado;
    public GameObject cabeza;
    public LogicaCabeza logicaCabeza;
    public bool estoyAgachado;
    //

    //ataque
    public bool estoyAtacando;
    public bool avanzoSolo;
    public float impulsoDeGolpe = 10;


    //ataque con espada
    public bool tengoArma;


    void Start()
    {
        puedoSaltar = false;
        anim = GetComponent<Animator>();

        velocidadInicial = velocidadMovimiento;
        velocidadAgachado = velocidadMovimiento * 0.5f;
    }

    void FixedUpdate() {
        //logica movimiento
        if (!estoyAtacando)
        {
            transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
            transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);
        }
        //logica golpe
        if (avanzoSolo) {
            rb.velocity = transform.forward * impulsoDeGolpe;
        }

    }

    // Update is called once per frame
    void Update()
    {
          ///
        if (Input.GetKey(KeyCode.LeftShift) && !estoyAgachado && puedoSaltar && !estoyAtacando)
        {
            velocidadMovimiento = velCorrer;
            if (y > 0)
            {
                anim.SetBool("correr", true);
            }
            else
            {
                anim.SetBool("correr", false);
            }
        }
        else
        {
            anim.SetBool("correr", false);
            if (estoyAgachado)
            {
                //Mover
                velocidadMovimiento = velocidadAgachado;
            }
            else if(puedoSaltar){
                //Mover
                velocidadMovimiento = velocidadInicial;
            }
        }

        //// FLECHAS
        //x = Input.GetAxis("Horizontal");
        //y = Input.GetAxis("Vertical");

        //JOYSTICKS
        x = left.GetTouchPosition.x;
        y = left.GetTouchPosition.y;

        if (Input.GetKeyDown(KeyCode.Return) && puedoSaltar && !estoyAtacando) {
            if (!tengoArma)
            {
                anim.SetTrigger("golpeo");
            }
            else
            {
                anim.SetTrigger("golpeEspada");
            }
            estoyAtacando = true;
        }

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        //logica salto
        if (puedoSaltar) {

            if (!estoyAtacando) {

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    anim.SetBool("salte", true);
                    rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
                }
                if (Input.GetKey(KeyCode.LeftControl))
                {
                    anim.SetBool("agachado", true);
                    //velocidadMovimiento = velocidadAgachado;

                    //cambio de collider
                    colAgachado.enabled = true;
                    colParado.enabled = false;

                    cabeza.SetActive(true);
                    estoyAgachado = true;
                }
                else
                {
                    if (logicaCabeza.contadorDeColision <= 0) {
                        anim.SetBool("agachado", false);
                        //velocidadMovimiento = velocidadInicial;

                        //cambio de collider
                        cabeza.SetActive(false);
                        colAgachado.enabled = false;
                        colParado.enabled = true;
                        estoyAgachado = false;
                    }
                }
            }

            anim.SetBool("tocoSuelo", true);
        }
        else {
            EstoyCayendo();
        }
    }
    public void EstoyCayendo()
    {
        //caer rapido
        rb.AddForce(fuerzaExtra * Physics.gravity);


        anim.SetBool("tocoSuelo", false);
        anim.SetBool("salte", false);
    }
    public void DejeDeGolpear() {
        estoyAtacando = false;
    }
    public void AvanzoSolo() {
        avanzoSolo = true;
    }
    public void DejoDeAvanzar() {
        avanzoSolo = false;
    }
    public void Salto() {
        if (puedoSaltar)
        {
            if (!estoyAtacando)
            {             
                    anim.SetBool("salte", true);
                    rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
       
            }

            anim.SetBool("tocoSuelo", true);
        }
        else
        {
            EstoyCayendo();
        }
    }
    public void MeAgache() {
        if (puedoSaltar)
        {

            if (!estoyAtacando)
            {
                    anim.SetBool("agachado", true);
                    //velocidadMovimiento = velocidadAgachado;

                    //cambio de collider
                    colAgachado.enabled = true;
                    colParado.enabled = false;

                    cabeza.SetActive(true);
                    estoyAgachado = true;
                
            }
            anim.SetBool("tocoSuelo", true);
        }
        else
        {
            EstoyCayendo();
        }
    }
    public void MePare() {
        if (puedoSaltar)
        {

            if (!estoyAtacando)
            {
                    if (logicaCabeza.contadorDeColision <= 0)
                    {
                        anim.SetBool("agachado", false);
                        //velocidadMovimiento = velocidadInicial;

                        //cambio de collider
                        cabeza.SetActive(false);
                        colAgachado.enabled = false;
                        colParado.enabled = true;
                        estoyAgachado = false;
                    }
            }

            anim.SetBool("tocoSuelo", true);
        }
        else
        {
            EstoyCayendo();
        }
    }
    public void Corriendo()
    {
        if (!estoyAgachado && puedoSaltar && !estoyAtacando)
        {
            velocidadMovimiento = velCorrer;
            if (y > 0)
            {
                anim.SetBool("correr", true);
            }
            else
            {
                anim.SetBool("correr", false);
            }
        }
    } 
    public void Caminando()
    {
            anim.SetBool("correr", false);
            if (estoyAgachado)
            {
                //Mover
                velocidadMovimiento = velocidadAgachado;
            }
            else if (puedoSaltar)
            {
                //Mover
                velocidadMovimiento = velocidadInicial;
            }
    }
    public void Ataque() {
        if (puedoSaltar && !estoyAtacando)
        {
            if (!tengoArma)
            {
                anim.SetTrigger("golpeo");
            }
            else
            {
                anim.SetTrigger("golpeEspada");
            }
            estoyAtacando = true;
        }
    }
}
