﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaPie : MonoBehaviour
{
    public LogicaPersonaje logicaPersonaje1;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "piso" || other.tag =="objeto")
        {
            logicaPersonaje1.puedoSaltar = true;
        }
    }
    private void OnTriggerExit(Collider o)
    {
        logicaPersonaje1.puedoSaltar = false;
    }
}
